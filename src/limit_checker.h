#pragma once

#include "common.h"
#include "io_redirect.h"

/* prototypes */
int check_ole(int from_fd, FILE *to_stream, int *total, int limit);
int check_tle(double *elapsed, int limit);
int check_mle(int *mem_peak, int limit);
