#include "limit_checker.h"

/***** public functions *****/

/* check OLE */
int check_ole(int from_fd, FILE *to_stream, int *total, int limit) {
    char buf[BUF_SIZE]; int bytes_read;

    // read from_fd then write to_stream
    while (is_fd_read_ready(from_fd)) {
        bytes_read = read(from_fd, buf, BUF_SIZE);
        if (bytes_read <= 0) break;
        fwrite(buf, bytes_read, 1, to_stream);
        *total += bytes_read;
        if ((limit && *total >= limit) || bytes_read < BUF_SIZE) break;
    }

    // return 1 limit exceed
    return limit && *total >= limit;
}

/* check TLE */
int check_tle(double *elapsed, int limit) {
    static struct timeval start, end;

    // initialize the start time
    if (!elapsed) {
        gettimeofday(&start, NULL);
        return 0;
    }

    // calculate time elapsed
    gettimeofday(&end, NULL);
    *elapsed = ((end.tv_sec - start.tv_sec) * 1E6 + (end.tv_usec - start.tv_usec)) / 1.0E6;

    // return 1 if limit exceed
    return limit && *elapsed >= limit;
}

/* check MLE */
int check_mle(int *mem_peak, int limit) {
    static char status_file[BUF_SIZE];

    // initialize the status path
    if (!mem_peak) {
        pid_t pid = limit;
        sprintf(status_file, "/proc/%d/status", pid);
        return 0;
    }

    // open status file
    FILE *fp = fopen(status_file, "r");
    if (!fp) return 0;

    // read proc status and find VmHWM
    char buf[BUF_SIZE]; int mem_use = 0;
    while (fgets(buf, sizeof(buf), fp)) {
        if (strncmp(buf, "VmHWM:", 6) == 0) {
            sscanf(buf + 6 + 1, "%d", &mem_use);
            mem_use *= 1024;    // turns KB to bytes
            *mem_peak = max(*mem_peak, mem_use);
            break;
        }
    }
    fclose(fp);

    // return 1 limit exceed
    return limit && *mem_peak >= limit;
}
