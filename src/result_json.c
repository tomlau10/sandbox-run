#include "result_json.h"

/* constants */
#define RES_FILE    "result.json"

/* result_json singleton */
struct submission *res;
static int cur_case;

/***** public functions *****/

/* init result json */
void init_result_json(int num_cases) {
    // init the result_json
    if (num_cases <= 0) num_cases = 1;
    cur_case = 0;
    res = malloc(num_cases * sizeof(struct submission));
}

/* update result json */
void update_result_json(struct submission *subm) {
    memcpy(res+cur_case, subm, sizeof(struct submission));
    cur_case++;
}

/* print result in json */
void print_result_json(struct user_config *ucfg) {
    // do not print if silence mode is on
    if (ucfg->silence_mode) return;

    // print to file or stdout
    FILE *to_stream = ucfg->redir_result ? fopen(RES_FILE, "w") : stdout;

    // set to fully buffered for flushing result json atomically
    setvbuf(to_stream, NULL, _IOFBF, 0);

    // print delimiter if json is print to stdout along with cmsg or child output
    if (!ucfg->redir_result && (!ucfg->redir_cmsg || ucfg->out == stdout))
        fprintf(to_stream, "%c", DELIM_VAL);

    // print result array
    int i;
    fprintf(to_stream, "{\n\"result\":[\"%s\"", strcode(res[0].ret));
    for (i=1; i<cur_case; i++) fprintf(to_stream, ",\"%s\"", strcode(res[i].ret));
    fprintf(to_stream, "],\n");

    // print stdout and stderr size array
    fprintf(to_stream, "\"stdout_size\":[%d", res[0].out_size);
    for (i=1; i<cur_case; i++) fprintf(to_stream, ",%d", res[i].out_size);
    fprintf(to_stream, "],\n");
    fprintf(to_stream, "\"stderr_size\":[%d", res[0].err_size);
    for (i=1; i<cur_case; i++) fprintf(to_stream, ",%d", res[i].err_size);
    fprintf(to_stream, "],\n");

    // print execute time and memory usage
    fprintf(to_stream, "\"execute_time\":[%lf", res[0].elapsed);
    for (i=1; i<cur_case; i++) fprintf(to_stream, ",%lf", res[i].elapsed);
    fprintf(to_stream, "],\n");
    fprintf(to_stream, "\"memory_usage\":[%d", res[0].mem_peak);
    for (i=1; i<cur_case; i++) fprintf(to_stream, ",%d", res[i].mem_peak);
    fprintf(to_stream, "]\n}\n");

    // close file if necessary
    if (ucfg->redir_result) fclose(to_stream);
}

/* convert return code to message string */
const char *strcode(int code) {
    // known return code
    switch (code) {
        case OK: return "OK";
        case CE: return "Compile Error";
        case ENF: return "Executable Not Found";
        case RE: return "Runtime Error";
        case TLE: return "Time Limit Exceed";
        case MLE: return "Memory Limit Exceed";
        case OLE: return "Output Limit Exceed";
        case SRE: return "System Resource Exceed";
        case BANNED: return "Bad System Call";
    }

    // other unknow error
    return "Judge Error";
}
