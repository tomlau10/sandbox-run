#pragma once
#include "common.h"
#include "io_redirect.h"
#include "limit_checker.h"
#include <seccomp.h>
#include <sys/prctl.h>
#include <sys/ptrace.h>
#include <sys/wait.h>

/* prototypes */
int monitor_program(struct user_config *ucfg, FILE *log_file, struct submission *subm);
