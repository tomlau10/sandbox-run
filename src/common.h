#pragma once

/* include library */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/resource.h>
#include <errno.h>

/* size of string buffer */
#define KB                  1024
#define MB                  1024*1024
#define BUF_SIZE            1024
#define DELIM_VAL           '\x00'

/* macro */
#define min(a,b)            ((a)<(b)?(a):(b))
#define max(a,b)            ((a)>(b)?(a):(b))
#define strcatf(s, ...)     sprintf(s + strlen(s), __VA_ARGS__)

/* log file macro */
#define lopen(enable, n, m) (enable ? fopen(n, m) : NULL)
#define lperror(fp, funcn)  (fp ? lprintf(fp, "error: %s: %s\n", funcn, strerror(errno)) : 0)
#define lprintf(fp, ...)    (fp ? fprintf(fp, __VA_ARGS__) : 0)
#define lclose(fp)          (fp ? fclose(fp) : 0)

/* status code */
enum {
    SRE=-2,
    OK=0, CE, ENF, RE, TLE, MLE, OLE, KILLED, BANNED,
    UNKNOWN=124, ERR
};

/* error tolerance level */
enum {
    ALL_ERROR = 0, TLE_CE_ONLY, CE_ONLY
};

/* data structure */
struct submission {
    double elapsed;
    int mem_peak, out_size, err_size, ret;
};

struct user_config {
    char **exec_file, *code_file;
    int enable_logging, silence_mode, redir_cmsg, redir_result;
    int error_lv, num_cases;
    rlim_t time_lim, mem_lim, out_lim;
    FILE *in, *out, *err;
};
