#include "parse_opts.h"

/* internal error code */
enum {
    SUCCESS = 0,
    REQ_USAGE,
    ERR_NOTOPT, ERR_NOARG, ERR_NOTNUM, ERR_NOEXEC
};

/***** private functions *****/

/* check is number */
static int is_num_str(char *s) {
    while (*s) if (!isdigit(*s++)) return 0;
    return 1;
}

/***** public functions *****/

/* display usage */
void display_usage() {
    // usage
    fprintf(stderr, "Usage:\n docker run [docker-options] sandbox-run [options] <executable> [args]\n\n");

    // gerneral options
    fprintf(stderr, "General Options:\n");
    fprintf(stderr, " -h          Display help, then exit\n");
    fprintf(stderr, " -c <file>   Source code file for compilation\n");
    fprintf(stderr, " -l          Enable logging, store monitor information\n");
    fprintf(stderr, " -s          Silence mode, suppress compile message and result json\n\n");

    // i/o setting options
    fprintf(stderr, "I/O Setting Options:\n");
    fprintf(stderr, " -I          Redirect stdin to files \"stdin[0-*]\",   default is read stdin\n");
    fprintf(stderr, " -O          Redirect stdout to files \"stdout[0-*]\", default is write stdout\n");
    fprintf(stderr, " -E          Redirect stderr to files \"stderr[0-*]\", default is write stderr\n");
    fprintf(stderr, " -C          Redirect compile message to \"cmsg.txt\", default is write stdout\n");
    fprintf(stderr, " -R          Redirect result json to \"result.json\",  default is write stdout\n\n");

    // monitor setting options
    fprintf(stderr, "Monitor Setting Options:\n");
    fprintf(stderr, " -e <level>  Error tolerance level, stop if test case encounter error\n"
                    "             0 = Stop on all errors (default)\n"
                    "             1 = Stop on Time Limit Exceed and Compile Error\n"
                    "             2 = Stop only if Compile Error\n");
    fprintf(stderr, " -n <cases>  Number of test input, default = 1 (0 = stdin closed)\n");
    fprintf(stderr, " -t <limit>  Time limit in second, default = 0 (unlimited)\n");
    fprintf(stderr, " -m <limit>  Memory limit in MB,   default = 0 (unlimited)\n");
    fprintf(stderr, " -o <limit>  Output limit in KB,   default = 0 (unlimited)\n\n");

    // online detailed usage page
    fprintf(stderr, "Visit Sandbox-Run Wiki page for detailed usage reference:\n");
    fprintf(stderr, "<https://bitbucket.org/tomlau10/sandbox-run/wiki/Home>\n\n");
    fprintf(stderr, "Visit Sandbox-Run Docker Hub page to give comments:\n");
    fprintf(stderr, "<https://registry.hub.docker.com/u/tomlau10/sandbox-run/>\n\n");
}

/* parse argv options */
int parse_argv(struct user_config *ucfg, int argc, char **argv) {
    // init the ucfg
    *ucfg = (struct user_config) {
        .num_cases = 1,
        .in = stdin,
        .out = stdout,
        .err = stderr
    };

    // get option
    char opt; int ret = SUCCESS;
    opterr = 0;     // suppress getopt() error message
    while (ret == SUCCESS && (opt = getopt(argc, argv, ":c:hlsIOECRe:n:t:m:o:")) != EOF) {
        switch (opt) {
            case 'h': ret = REQ_USAGE; break;
            case 'c': ucfg->code_file = optarg; break;
            case 'l': ucfg->enable_logging = 1; break;
            case 's': ucfg->silence_mode = 1; break;
            case 'I': ucfg->in = NULL; break;
            case 'O': ucfg->out = NULL; break;
            case 'E': ucfg->err = NULL; break;
            case 'C': ucfg->redir_cmsg = 1; break;
            case 'R': ucfg->redir_result = 1; break;
            case 'e': case 'n': case 't': case 'm': case 'o':
                if (!is_num_str(optarg)) ret = ERR_NOTNUM;
                else switch (opt) {
                    case 'e': ucfg->error_lv = atoi(optarg); break;
                    case 'n': ucfg->num_cases = atoi(optarg); break;
                    case 't': ucfg->time_lim = atoi(optarg); break;
                    case 'm': ucfg->mem_lim = atoi(optarg)*MB; break;
                    case 'o': ucfg->out_lim = atoi(optarg)*KB; break;
                }
                break;
            case ':': ret = ERR_NOARG; break;   // missing argument
            default: ret = ERR_NOTOPT;          // invalid option
        }
    }

    // get exec_file and args
    if (ret == SUCCESS) {
        if (!argv[optind]) ret = ERR_NOEXEC;
        else ucfg->exec_file = argv + optind;
    }

    // print error as necessary
    switch (ret) {
        case ERR_NOTOPT:
            fprintf(stderr, "%s: -%c: invalid option\n", argv[0], optopt);
            break;
        case ERR_NOARG:
            fprintf(stderr, "%s: -%c: missing <argument>\n", argv[0], optopt);
            break;
        case ERR_NOTNUM:
            fprintf(stderr, "%s: -%c: \"%s\" is not a number\n", argv[0], opt, optarg);
            break;
        case ERR_NOEXEC:
            fprintf(stderr, "%s: missing <executable>\n", argv[0]);
            break;
    }

    // return
    return ret;
}
