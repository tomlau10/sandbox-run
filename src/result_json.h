#pragma once

#include "common.h"

/* prototypes */
void init_result_json(int num_cases);
void update_result_json(struct submission *subm);
void print_result_json(struct user_config *ucfg);
const char *strcode(int code);
