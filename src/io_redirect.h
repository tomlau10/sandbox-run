#pragma once

#include "common.h"
#include <fcntl.h>
#include <pty.h>
#include <termios.h>
#include <sys/select.h>

/* constants */
#define R_END 0     // read end of a pipe
#define W_END 1     // write end of a pipe

/* prototypes */
FILE *redirect_to_file(FILE **fp, char *name, int i, char *mode);
int is_fd_read_ready(int fd);
int is_fd_write_ready(int fd);
int pipe_input_to_child();
pid_t fork_with_pipes(int pipes[STDERR_FILENO+1][2], struct user_config *ucfg);
void close_redirector(int eot);
