#include "compile_code.h"

/* constants */
#define CMSG_FILE   "cmsg.txt"
#define NULL_FILE   "/dev/null"
#define TIMEOUT_S   2

/* private variable */
static char cmd[BUF_SIZE];
static struct user_config *ucfg;

/***** private functions *****/

/* return the desired stdout file name */
static const char *stdout_name() {
    if (ucfg->silence_mode) return NULL_FILE;
    if (ucfg->redir_cmsg) return CMSG_FILE;
    return "&1";    // stdout file descriptor
}

/* compile c */
static int compile_c() {
    sprintf(cmd, "timeout -t %d gcc -Wall -Wfatal-errors %s -o %s >%s 2>&1",
        TIMEOUT_S, ucfg->code_file, ucfg->exec_file[0], stdout_name());
    return system(cmd);
}

/* compile python */
static int compile_py() {
    sprintf(cmd, "timeout -t %d python -m py_compile %s >%s 2>&1",
        TIMEOUT_S, ucfg->code_file, stdout_name());
    if (system(cmd) != 0) return 1;
    sprintf(cmd, "echo \"#! $(which python)\" | cat - %s > %s",
        ucfg->code_file, ucfg->exec_file[0]);
    return system(cmd);
}

/* compile ruby */
static int compile_rb() {
    sprintf(cmd, "timeout -t %d ruby -c %s >%s 2>&1",
        TIMEOUT_S, ucfg->code_file, stdout_name());
    if (system(cmd) != 0) return 1;
    sprintf(cmd, "echo \"#! $(which ruby)\" | cat - %s > %s",
        ucfg->code_file, ucfg->exec_file[0]);
    return system(cmd);
}

/* compile javascript */
static int compile_js() {
    // javascript does not have compiler
    if (!ucfg->silence_mode && ucfg->redir_cmsg && system("touch " CMSG_FILE) != 0)
        return 1;
    sprintf(cmd, "echo \"#! $(which node)\" | cat - %s > %s",
        ucfg->code_file, ucfg->exec_file[0]);
    return system(cmd);
}

/***** public functions *****/

/* compile code */
int compile_code(struct user_config *user_cfg) {
    // reference to user_config
    ucfg = user_cfg;

    // only compile if code_file is specified
    if (ucfg->code_file) {
        // get file extension and call correponding compile command
        int ret = 0, len = strlen(ucfg->code_file);
        if (strcmp(ucfg->code_file + len - 2, ".c") == 0) {
            ret = compile_c();
        } else if (strcmp(ucfg->code_file + len - 3, ".py") == 0) {
            ret = compile_py();
        } else if (strcmp(ucfg->code_file + len - 3, ".js") == 0) {
            ret = compile_js();
        } else if (strcmp(ucfg->code_file + len - 3, ".rb") == 0) {
            ret = compile_rb();
        } else {    // unknown language
            fprintf(stderr, "Unknown source code file extension\n");
            ret = 1;
        }

        // print delimiter if both cmsg and chlid output are print to stdout
        if (!ucfg->silence_mode && !ucfg->redir_cmsg) {
            if (ucfg->out == stdout)
                fprintf(stdout, "%c", DELIM_VAL);
            if (ucfg->err == stderr)
                fprintf(stderr, "%c", DELIM_VAL);
        }

        // compile error if anything went wrong
        if (ret) return CE;
    }

    // check if the exec_file exist
    if (access(ucfg->exec_file[0], F_OK) < 0) return ENF;

    // finally add execute permission then finish
    sprintf(cmd, "chmod +x %s", ucfg->exec_file[0]);
    system(cmd);
    return OK;
}
