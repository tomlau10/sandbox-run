#include "io_redirect.h"

/***** private variables *****/

static int use_delimiter = 0;
static FILE *from_stream = NULL, *to_stream = NULL;

/***** private functions *****/

/* set a fd to non blocking mode */
void set_nonblock(int fd) {
    int flags = fcntl(fd, F_GETFL, 0);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}

/***** public functions *****/

/* redirect stream to file with name tag */
FILE *redirect_to_file(FILE **fp, char *name, int i, char *mode) {
    char buf[BUF_SIZE];
    sprintf(buf, "%s%d", name, i);
    return *fp = fopen(buf, mode);
}

/* check if fd is ready to read */
int is_fd_read_ready(int fd) {
    // set the fd_set and zero timeout
    fd_set set; FD_ZERO(&set); FD_SET(fd, &set);
    struct timeval timeout = (struct timeval){0};

    // call select and return if fd is set
    select(fd+1, &set, NULL, NULL, &timeout);
    return FD_ISSET(fd, &set);
}

/* check if fd is ready to write */
int is_fd_write_ready(int fd) {
    // set the fd_set and zero timeout
    fd_set set; FD_ZERO(&set); FD_SET(fd, &set);
    struct timeval timeout = (struct timeval){0};

    // call select and return if fd is set
    select(fd+1, NULL, &set, NULL, &timeout);
    return FD_ISSET(fd, &set);
}

/* pipe test case input to child */
int pipe_input_to_child() {
    // read from_stream then write to_stream
    int c, has_data = 0;
    while ((c = fgetc(from_stream)) >= 0) {
        // delimit character is \x00
        if (use_delimiter && c == DELIM_VAL) {
            break;
        }

        if (fputc(c, to_stream) != c) {
            // if cannot write, put the char back
            ungetc(c, from_stream);
            break;
        } else {
            // has data, need to flush
            has_data = 1;
        }
    }

    // flush if has data
    if (has_data) {
        fflush(to_stream);
    }

    // return 1 if end of test case
    if ((c == EOF && feof(from_stream)) || (use_delimiter && c == DELIM_VAL)) {
        fclose(to_stream);
        to_stream = NULL;
        return 1;
    }

    // return 0 if not end of test case
    return 0;
}

/* fork child and create pipes for redirection */
pid_t fork_with_pipes(int pipes[STDERR_FILENO+1][2], struct user_config *ucfg) {
    // create pipes for stdin, stderr, stdout will use terminal device
    int master_fd;
    pipe(pipes[STDIN_FILENO]);
    pipe(pipes[STDERR_FILENO]);

    // fork child using forkpty to prepare for line buffered stdout
    pid_t pid = forkpty(&master_fd, NULL, NULL, NULL);
    if (pid == -1) {
        // fork error
        return -1;
    } else if (pid == 0) {
        // child, redirect stdin, stderr to pipes
        dup2(pipes[STDIN_FILENO][R_END], STDIN_FILENO);
        close(pipes[STDIN_FILENO][W_END]);

        dup2(pipes[STDERR_FILENO][W_END], STDERR_FILENO);
        close(pipes[STDERR_FILENO][R_END]);
        return 0;
    }

    // parent, use the line buffered terminal fd
    pipes[STDOUT_FILENO][R_END] = master_fd;
    pipes[STDOUT_FILENO][W_END] = -1;

    // close unused pipe ends
    close(pipes[STDIN_FILENO][R_END]);
    close(pipes[STDERR_FILENO][W_END]);

    // set terminal attribute
    struct termios term;
    tcgetattr(master_fd, &term);
    term.c_oflag &= ~ONLCR;     // disable \r\n line breaking
    tcsetattr(master_fd, TCSANOW, &term);

    // setup stream redirection
    from_stream = ucfg->in;
    to_stream = fdopen(pipes[STDIN_FILENO][W_END], "w");
    set_nonblock(fileno(from_stream));
    set_nonblock(fileno(to_stream));

    // check if delimiter should be used
    use_delimiter = ucfg->num_cases > 1 && from_stream == stdin;

    return pid;
}

/* close the redirector and prepare stdin for next test case */
void close_redirector(int eot) {
    // if not end of testcase and delimiter is used
    if (!eot && from_stream == stdin && use_delimiter) {
        // read until next stdin
        while (1) {
            int c = fgetc(from_stream);
            if ((c == EOF && feof(from_stream)) || c == DELIM_VAL)
                break;
        }
    }
    from_stream = NULL;

    // close to_stream
    if (!eot && to_stream) {
        fclose(to_stream);
    }
    to_stream = NULL;
}
