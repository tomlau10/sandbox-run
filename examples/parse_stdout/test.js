/* require library */
var cp = require('child_process');

/* convert window format path to posix format */
function posixPath(dir) {
    if (dir.indexOf(':') > -1) {
        dir = dir[0].toLowerCase() + dir.slice(1);
        dir = '/' + dir.replace(':', '').replace(/\\/g, '/');
    }
    return dir;
}

/* build docker command */
var security_option = process.platform === 'linux' ? '--security-opt apparmor:unconfined' : '';
var sandbox_mount = posixPath(__dirname) + ':/vol';
var setuid_option = process.platform === 'linux' ? '-u $(id -u):$(id -g)' : '';
function makeRunCmd(flags) {
    return [
        'docker', 'run', '-i',  // docker run with interactive mode
        '--rm',                 // auto remove
        security_option,        // fix ptrace with apparmor problem in ubuntu
        '-v', sandbox_mount,    // sandbox mount
        setuid_option,          // set uid properly on ubuntu
        'sandbox-run',          // the sandbox run image
        flags,                  // additional flags
        '-c', 'hello.c',        // source code file
        './binary'              // executable path
    ].join(' ');
}

/* run sandbox */
function run(id, flags, callback) {
    var cmd = makeRunCmd(flags);
    var stdout = cp.execSync(cmd).toString();
    console.log(id + ':\n' + cmd + '\n');    
    callback(stdout);
    console.log('=====');
}

/* default setting, stdout will be [ cmsg \x00 stdout \x00 json ] */
run('default', '', function (stdout) {
    // get offset
    var stdoutOffset = stdout.indexOf('\x00');
    var resultOffset = stdout.lastIndexOf('\x00');
    
    // parse stdout
    var cmsg = stdout.substring(0, stdoutOffset-1);
    var resultJSON = stdout.substring(resultOffset+1, stdout.length);
    var resultObj = JSON.parse(resultJSON);
    var output = stdout.substr(stdoutOffset+1, resultObj.stdout_size[0]);
    console.log('cmsg: ' + cmsg + '\n');
    console.log('stdout: ' + output + '\n');
    console.log('json: ' + resultJSON + '\n');
});

/* redirect cmsg, stdout will be [ stdout \x00 json ] */
run('redirect cmsg', '-C', function (stdout) {
    // get offset
    var resultOffset = stdout.lastIndexOf('\x00');
    
    // parse stdout
    var resultJSON = stdout.substring(resultOffset+1, stdout.length);
    var resultObj = JSON.parse(resultJSON);
    var output = stdout.substr(0, resultObj.stdout_size[0]);
    console.log('stdout: ' + output + '\n');
    console.log('json: ' + resultJSON + '\n');
});

/* redirect cmsg and stdout, stdout will be [ json ] */
run('only json', '-CO', function (stdout) {
    // parse stdout
    var resultJSON = stdout;
    var resultObj = JSON.parse(resultJSON);
    console.log('json: ' + resultJSON + '\n');
});

/* silent mode, stdout will be [ stdout ] */
run('silent mode', '-s', function (stdout) {
    // parse stdout
    console.log('stdout: ' + stdout + '\n');
});
