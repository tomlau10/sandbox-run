#include <stdio.h>
#include <unistd.h>

int main() {
    int a, b;
    while (scanf("%d%d", &a, &b) == 2) {
        if (a == b) {
            printf("ready to call fork()\n");
            fork();
        }
        printf("a + b = %d\n", a + b);
    }
    printf("finish\n");
    return 0;
}
